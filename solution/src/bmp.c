#include "../include/bmp.h"
#include <malloc.h>

static enum write_status create_bmp(struct image const *img, struct bmp_header *header) {
    size_t padding = (size_t) (4 - img->width * (BitCount / 8) % 4);

    header->bfType = Type;
    header->biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header-> bfileSize = header->biSizeImage + sizeof(struct bmp_header);
    header->bfReserved = Reserved;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = Header_Size;
    header->biHeight = img->height;
    header->biWidth = img->width;
    header->biPlanes = Planes;
    header->biBitCount = BitCount;
    header->biCompression = Compression;
    header->biXPelsPerMeter = PelsPerMeter;
    header->biYPelsPerMeter = PelsPerMeter;
    header->biClrUsed = ColorUsed;
    header->biClrImportant = ColorImportant;

    return WRITE_OK;
}

static size_t take_padding(size_t width);

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};

    if (!in) { return READ_ERR; }

    fseek(in, 0, SEEK_END);
    size_t f_size = ftell(in);
    if (f_size < sizeof(struct bmp_header)) { return READ_INVALID_HEADER; }
    rewind(in);

    fread(&header, sizeof(struct bmp_header), 1, in);

    if (!img) { return READ_ERR; }
    img->height = header.biHeight;
    img->width = header.biWidth;
    fseek(in, header.bOffBits, SEEK_SET);

    if (image_malloc_data(img)) { return READ_ERR; }

    size_t padding = take_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        fread(img->sum_pixel_of_image + i * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, in);
        fseek(in, (int) padding, SEEK_CUR);
    }
    return READ_OK;
}

static inline size_t take_padding(const size_t width) {
    return (4 - width * (BitCount / 8) % 4);
}

enum write_status to_bmp(FILE *out, struct image const*img) {

    if (!img) { return WRITE_ERROR; }
    struct bmp_header header = {0};
    create_bmp(img, &header);

    if (!out) { return WRITE_ERROR; }
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    fseek(out, header.bOffBits, SEEK_SET);

    size_t padding = take_padding(img->width);
    uint8_t *arr_padd = malloc(padding);

    if (!arr_padd)
        return WRITE_ERROR;

    for (size_t i = 0; i < padding; ++i)
        *(arr_padd + i) = 0;

    if (img->sum_pixel_of_image != NULL){
        for (size_t i = 0; i < img->height; ++i) {
            fwrite(img->sum_pixel_of_image + i * img->width, img->width * sizeof(struct pixel), 1, out);
            fwrite(arr_padd, padding, 1, out);
        }
    }

    free(arr_padd);
    return WRITE_OK;
}

