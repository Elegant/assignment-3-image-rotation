#include "../include/picture.h"

#include <malloc.h>

bool image_malloc_data(struct image *image_store){
    uint32_t height = image_store->height;
    uint32_t width = image_store->width;

    image_store->sum_pixel_of_image = malloc(sizeof(struct pixel) * width * height);

    return image_store->sum_pixel_of_image == NULL;
}

void free_img(struct image *image_store){ free(image_store->sum_pixel_of_image); }
