#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        return 1;
    }
    char *input_fn = NULL, *output_fn = NULL;
    input_fn = argv[1];
    output_fn = argv[2];

    FILE *input_file = NULL;
    FILE *output_file = NULL;

    if (check_open_file(&input_file, input_fn, "rb")){
        fprintf(stderr, "Couldn't open input file");
        return 1;

    }
    if (check_open_file(&output_file, output_fn, "wb")){
        check_close_file(&input_file);
        fprintf(stderr, "Couldn't open output file");
        return 1;
        
    }

    struct image image = {0};
    if (from_bmp(input_file, &image)){
        check_close_file(&input_file);
        check_close_file(&output_file);
        free_img(&image);
        fprintf(stderr, "Error occured while trying to read the input file");
        return 1;
    }
    struct image new_image = rotate(image);
    if (to_bmp(output_file, &new_image)){
        check_close_file(&input_file);
        check_close_file(&output_file);
        free_img(&image);
        free_img(&new_image);
         fprintf(stderr, "Error occured while trying to write to the output file");
        return 1;
    }

    if (check_close_file(&input_file)){
        free_img(&image);
        free_img(&new_image);
         fprintf(stderr, "Couldn't close the input file");
        return 1;
    }
    if (check_close_file(&output_file)){
        free_img(&image);
        free_img(&new_image);
        fprintf(stderr, "Couldn't close the output file");
        return 1;
    }

    free_img(&image);
    free_img(&new_image);
    return 0;
}
