#include "../include/rotate.h"

struct image rotate( struct image const source){
    struct pixel *temp_pixel = malloc(sizeof(struct pixel) * source.height * source.width);

    struct pixel *data = source.sum_pixel_of_image;

    for (uint32_t i = 0; i < source.height; i++) {
        for (uint32_t j = 0; j < source.width; j++) {
            *(temp_pixel + j * source.height + (source.height - 1 - i)) = *(data + i * source.width + j);
        }
    }
    struct image img = {0};
    img.width = source.height;
    img.height = source.width;
    img.sum_pixel_of_image = temp_pixel;
    return img;
}

