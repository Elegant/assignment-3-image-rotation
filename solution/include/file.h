#ifndef ROTATE_FILE_H
#define ROTATE_FILE_H

#include <stdbool.h>
#include <stdio.h>

bool check_open_file(FILE **file, const char *name, const char *mode);

bool check_close_file(FILE **file);

#endif
