#ifndef ROTATE_BMP_H
#define ROTATE_BMP_H
#include <stdint.h>
#include <stdio.h>

#include "picture.h"

static size_t BitCount = 24;
static uint32_t Type = 19778;
static uint32_t Reserved = 0;
static uint32_t Header_Size = 40;
static uint16_t Planes = 1;
static uint32_t Compression= 0;
static uint32_t PelsPerMeter = 2834;
static uint32_t ColorUsed = 0;
static uint32_t ColorImportant = 0;

#pragma pack(push, 1)

struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERR
};

enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
